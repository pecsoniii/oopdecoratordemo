
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ReceiptFactory {

    //Todo make these private?  --Change them back?
    private String header; //line containing "Best Buy", store_num, street_addr, phone
    private String state_code;
    private TaxComputation[] taxComputationsObjs = new TaxComputation[4]; //Tax computation object (for each state)
    private AddOn[] addOns; // Secondary Header, rebate and coupon add-ons

    public ReceiptFactory() throws IOException {

        //Instantiate State Tax Computation objects of type TaxComputation
        TaxComputation mdTax = new MDTaxComputation();
        TaxComputation caTax = new CATaxComputation();
        TaxComputation deTax = new DETaxComputation();
        TaxComputation maTax = new MATaxComputation();

        //Populate an array of TaxComputation so that a particular state tax can be applied as needed
        this.taxComputationsObjs[0] = mdTax;
        this.taxComputationsObjs[1] = caTax;
        this.taxComputationsObjs[2] = deTax;
        this.taxComputationsObjs[3] = maTax;

        //!!! To demonstrate different states, enter a number here to select a different store location

        System.out.println("***NOTE:  FOR DEMONSTRATION PURPOSES SELECT A STORE***\nWhich store location are you ordering from?\n" +
                "Enter 0 for Maryland\nEnter 1 for California\nEnter 2 for Delaware\nEnter 3 for Massachusetts ");
        Scanner scanner = new Scanner(System.in);
        int stateChoice = scanner.nextInt();
        if(stateChoice==0) header = readFile("configMD.txt");
        else if (stateChoice==1) header = readFile("configCA.txt");
        else if (stateChoice==2) header = readFile("configDE.txt");
        else if (stateChoice==3) header = readFile("configMA.txt");

        //Scan the configuration file TODO TEST
        Scanner scan = new Scanner(header);

        //Sets the state code, assuming the first element of configured list is the state code
        this.state_code = scan.next();

        //Sets the header based off the configuration
        this.header = "\nBest Buy\n"
                + "Address: "
                + scan.nextLine()
                + scan.nextLine()
                + "\nPhone Number :  "
                + scan.nextLine()
                + "\nStore Number:  "
                + scan.nextLine();

    }

    public Receipt getReceipt(PurchasedItems items, String date) {
        //***Decorator Design Pattern***//

        //Instantiate a BasicReceipt object (What is to be decorated) of type Receipt
        Receipt thisReceipt = new BasicReceipt(items);

        //Set the date
        ((BasicReceipt) thisReceipt).setDate(date);
        //Set the TaxComputation specific to the state code
        ((BasicReceipt) thisReceipt).setTaxComputation(taxComputationsObjs[Integer.valueOf(state_code)]);
        //Set the store information to be displayed by the receipt
        ((BasicReceipt) thisReceipt).setStoreInfo(header);

        //Secondary heading
        String checkDate = date.substring(5);
        //If the date is a holiday, decorate with HolidayGreeting
        if (checkDate.equals("12-25") || checkDate.equals("12-31") || checkDate.equals("01-01")) {
            thisReceipt = new PreDecorator(new HolidayGreeting(), thisReceipt);
        }
        //Else if it is a summer sale day (August 13, or August 14), decorate with Summer Sale Greeting
        else if (checkDate.equals("08-13") || checkDate.equals("08-14")) {
            thisReceipt = new PreDecorator(new SummerSaleGreeting(), thisReceipt);
        }
        //Else Standard Greeting
        else {
            thisReceipt = new PreDecorator(new StandardGreeting(), thisReceipt);
        }

        //Coupon Decorator, if statement checks to see if condition applies
        if (new Coupon100Get10Percent().applies(items)) {
            thisReceipt = new PostDecorator(new Coupon100Get10Percent(), thisReceipt);
        }

        //Rebate Decorator, if statement checks to see if the condition applies
        if (new Rebate1406().applies(items))
            thisReceipt = new PostDecorator(new Rebate1406(), thisReceipt);

        //Returns the Receipt as a part of the factory method
        return thisReceipt;

    }

    //My favorite file to string solution by user Dónal
    //Source: https://stackoverflow.com/questions/326390/how-do-i-create-a-java-string-from-the-contents-of-a-file
    private String readFile(String pathname) throws IOException {
        File newFile = new File(pathname);

        File file = new File(newFile.getAbsolutePath());
        StringBuilder fileContents = new StringBuilder((int) file.length());

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine() + System.lineSeparator());
            }
            return fileContents.toString();
        }
    }
}
