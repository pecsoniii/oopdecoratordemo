public interface AddOn{
    boolean applies(PurchasedItems items); //Passed items contains all objects for the current receipt
    String getLines(); //Returns the added lines of text to be printed at a single string
}
