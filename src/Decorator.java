public abstract class Decorator implements Receipt{

    //Trailer is a receipt, that prints the receipt if callTrailer() is called
    private Receipt trailer;

    public Decorator(Receipt r){
        trailer = r;
    }

    protected void callTrailer(){
        trailer.prtReceipt();
    }

    public abstract void prtReceipt();

}
