public abstract class TaxComputation {
    public abstract double computeTax(PurchasedItems items, String date);
    public abstract boolean taxHoliday();
}
