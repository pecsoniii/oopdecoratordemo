public class StandardGreeting implements AddOn, SecondaryHeading {
    public boolean applies(PurchasedItems items){
        return true; //SecondaryHeading decorator is always applied
    }
    public String getLines(){
        return "*Thanks for shopping at Best Buy!!!\n" +
                "*Here is the store information followed by your receipt*";
    }
}
