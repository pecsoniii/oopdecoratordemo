//Controls the behavior of the Header
public class PreDecorator extends Decorator {

    private AddOn a;

    //Takes in the decoration, and receipt
    public PreDecorator(AddOn a, Receipt r){
        super(r);
        this.a=a;
    }

    //Gives precedence to the decoration
    public void prtReceipt(){
        System.out.println(a.getLines());
        callTrailer();
    }
}
