public class DETaxComputation extends TaxComputation {
    public double computeTax(PurchasedItems items, String date){
        if(taxHoliday()){/*Tax holiday conditions here*/}
        return 0.0; //Placeholder return statement
    }
    public boolean taxHoliday(){
        //Delaware does not have tax holidays for computers and computer accessories
        return false; //There will never be a taxHoliday
    }
}