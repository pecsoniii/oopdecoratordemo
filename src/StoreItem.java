import java.util.ArrayList;

public class StoreItem {

    //Item information
    private String itemCode; //e.g. 3010
    private String itemDescription; //e.g. Dell Laptop
    private String ItemPrice;

    //Constructor
    public StoreItem(String code, String descript, String price) {
        this.itemCode = code;
        this.itemDescription = descript;
        this.ItemPrice = price;
    }

    //Getters and Setters

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemPrice() {
        return ItemPrice;
    }

    public void setItemPrice(String itemPrice) {
        ItemPrice = itemPrice;
    }

}