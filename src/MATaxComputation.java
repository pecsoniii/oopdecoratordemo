public class MATaxComputation extends TaxComputation {
    private String date;
    public double computeTax(PurchasedItems items, String date){
        this.date=date;
        if(taxHoliday())
            return 0;
        else
            return items.getTotalCost()*.0625; //Placeholder return statement
    }
    //TODO ACCOUNT FOR TAX HOLIDAY
    public boolean taxHoliday(){
        if (date.substring(5).equals("08-13")||date.substring(5).equals("08-14"))
            return true;
        else
            return false;
    }
}