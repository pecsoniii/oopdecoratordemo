import java.util.Date;

public class BasicReceipt implements Receipt {

    private String storeInfo; //Store number, store address, phone number

    //Instance of a Purchased Item collection
    private PurchasedItems items;
    //The user inputted date
    private String date;
    //TaxComputation object
    private TaxComputation tc;

    //Concstructor
    public BasicReceipt(PurchasedItems items) {
        this.items = items;
    }

    public void setStoreInfo(String header) {
        this.storeInfo = header;
    }

    public void setTaxComputation(TaxComputation tc) {
        this.tc = tc;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String getDate() {
        return date;
    }

    //Prints the contents of the Receipt with relevant information and in a user friendly view
    @Override
    public void prtReceipt() {
        int index = 0;
        System.out.println(storeInfo);
        System.out.println("\nThe date is:  " + getDate() + "\n");
        System.out.println("********************************************************");
        while (items.hasNext(index)) {
            System.out.print(
                    "\nItem:  " + items.getItem(index).getItemDescription() + "\n" +
                            "Code:  " + items.getItem(index).getItemCode() + "\n" +
                            "Price:  " + items.getItem(index).getItemPrice() + "\n\n"

            );
            index++;
        }
        System.out.println("The total cost of your purchases:  " + items.getTotalCost());
        System.out.println("Your total cost after taxes is:  " +
                new java.text.DecimalFormat("0.00").format(items.getTotalCost() + tc.computeTax(items, date)) +
                "\n\n********************************************************\n\n");
    }
}
