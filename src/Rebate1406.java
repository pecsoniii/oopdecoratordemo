public class Rebate1406 implements AddOn, Rebate {
    public boolean applies(PurchasedItems items) {
        return items.containsItem("1406");
    }

    public String getLines() {
        return "Fill this form out and mail to the provided address.\nMail-in Rebate for Item #1406\n\n" + "Name:\n"                 + "Address:\n\n"
                + "Mail to: Best Buy Rebates, P.O. Box 1400, Orlando, FL";
    }

    //This looks to see if the item is of code 1406, if so a rebate is applied
}
