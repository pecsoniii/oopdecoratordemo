import java.io.IOException;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) throws IOException {

        Scanner scan = new Scanner(System.in);

        //Creates a PurchasedItems object.
        PurchasedItems items = new PurchasedItems();

        //Constructs ReceiptFactory object.
        System.out.print("What is the date?  \n" +
                "***Note:  Special Holiday Greetings are available on December 25, 31 and January 1!***\n" +
                "***Note:  Special Summer Sales available during August 13 and 14!***\n" +
                "Please enter in the following format:  YYYY-MM-DD\n");
        String today = scan.nextLine();


        //TODO implement user input for date entry
        // 3.	Prompts user for items to purchase, storing each in PurchasedItems.
        System.out.print("Select your items!\nEnter the number of the item you would like to purchase." +
                "You can make as many purchases as you want.  When you are done shopping enter x to exit!");
        //Test
        StoreItem laptop = new StoreItem("1406", "Dell Laptop", "199.99");
        StoreItem desktop = new StoreItem("999", "Dell Desktop", "2000.00");
        StoreItem candy = new StoreItem("111", "Snickers Bar", "2.00");
        String usrContinues = "";

        while (!usrContinues.equals("x")) {
            System.out.println("Here is a list of items for you to choose from:  \n" +
                    "1.  Laptop\n" +
                    "2.  Desktop\n" +
                    "3.  Maybe some candy for your travels?");
            usrContinues = scan.nextLine();
            switch (usrContinues) {
                case "1":
                    items.addItem(laptop);
                case "2":
                    items.addItem(desktop);
                case "3":
                    items.addItem(candy);
            }

            System.out.println("Would you like to continue shopping?\n" +
                    "Enter x to exit, to keep shopping enter anything any other key");
            usrContinues = scan.nextLine();

        }

        ReceiptFactory factory = new ReceiptFactory();
        Receipt receipt = factory.getReceipt(items, today);
        receipt.prtReceipt();

    }
}
