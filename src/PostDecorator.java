//Controls the behavior of the Header
public class PostDecorator extends Decorator{

    private AddOn a;

    //Takes in the decoration, and receipt
    public PostDecorator (AddOn a, Receipt r){
        super(r);
        this.a=a;
    }

    //Calles the decoration following the BasicReceipt body
    public void prtReceipt(){
        callTrailer();
        System.out.println(a.getLines());
    }

}
