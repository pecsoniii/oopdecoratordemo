public class Coupon100Get10Percent implements AddOn, Coupon{

    public boolean applies(PurchasedItems items){
        return items.getTotalCost()>100; //Checks to see if items costs exceeds 100.  If so, coupon applies
    }

    public String getLines(){
        return "\nYour purchase exceeds 100 dollars, here is a ten percent coupon! For use on your next visit :)\n";
    }
}
