import java.util.ArrayList;

public class PurchasedItems {

    //Arraylist Collection stores the different items of a receipt
    private ArrayList<StoreItem> items;

    public PurchasedItems(){
        items = new ArrayList();
    }

    //Add item to items
    public void addItem(StoreItem item){
        items.add(item);
    }

    //Get the total cost of the items, return double totalCost
    //totalCost calculated by iteration, item price at index is converted to a double
    public double getTotalCost(){

        double totalCost = 0.0;
        for(int i = 0; i < items.size(); i++){
              totalCost += Double.valueOf(items.get(i).getItemPrice());
        }
        return totalCost;
    }

    //contains is returned true if during iteration an item with the corresponding itemCode
    public boolean containsItem(String itemCode){
        boolean contains = false;
        for(int i = 0; i < items.size(); i++){
            if(items.get(i).getItemCode().equals(itemCode)){
                contains = true;
            }
        }
        return contains;
    }

    //Return a particular item at a give index
    public StoreItem getItem(int index){
        StoreItem thisItem = items.get(index);
        return  thisItem;
    }

    //Return boolean hasNext
    public boolean hasNext(int index){
        return items.size()>index;
    }

}


