public class CATaxComputation extends TaxComputation {

    public double computeTax(PurchasedItems items, String date){
        if(taxHoliday()){/*Tax holiday conditions here*/}
        return items.getTotalCost()*.075; //Placeholder return statement
    }

    public boolean taxHoliday(){
        //California does not have tax holidays for computers and computer accessories
        return false; //There will never be a taxHoliday
    }

}
